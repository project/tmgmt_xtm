Docker-based Drupal stack
==========

Ready to use drupal docker instance.
If you would like to build a Drupal environment ready to use with our module, follow the steps below.

Table of contents
-----------------

- [Deploying-and-running](#deploying-and-running)
    - [Drupal build](#drupal-build)
    - [Run environment with Docker Compose](#run-environment-with-docker-compose)
    - [Drupal installation](#drupal-installation)
    - [Modules installation](#modules-installation)
    - [Translation environment configuration](#translation-environment-configuration)

-------------------------------------------------------------------------------

Deploying and running
---------------------

### Drupal build

```
# Create new recommended drupal project | --ignore-platform-reqs parameter is optional
composer create-project drupal/recommended-project my-project --ignore-platform-reqs

cd my-project

# Get latest docker4drupal stack and unpack it.
# Keep in mind it can be necessary to update version of downloaded stack
wget -qO- https://github.com/wodby/docker4drupal/releases/download/6.0.12/docker4drupal.tar.gz | tar xvz

# Delete compose.override.yml as it's used to deploy vanilla Drupal
rm compose.override.yml
```

Ensure database access settings in your default.settings.php (settings.php for some versions of drupal) corresponds to values in .env file, e.g.:
```
$databases['default']['default'] = array (
'database' => 'drupal', // same as $DB_NAME
'username' => 'drupal', // same as $DB_USER
'password' => 'drupal', // same as $DB_PASSWORD
'host' => 'mariadb', // same as $DB_HOST
'driver' => 'mysql',  // same as $DB_DRIVER
'port' => '3306', // different for PostgreSQL
'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql', // different for PostgreSQL
'prefix' => '',
);
```

Traefik container used for routing. By default, we use port 8000 to avoid potential conflicts but if port 80 is free on your host machine just replace traefik's ports definition in the compose file.

By default, BASE_URL set to drupal.docker.localhost, you can change it in .env file.

Add 127.0.0.1 drupal.docker.localhost to your /etc/hosts file (some browsers like Chrome may work without it). Do the same for other default domains.

For more information regarding installation of recommended drupal instance please visit this [`link`](https://www.drupal.org/docs/develop/using-composer/starting-a-site-using-drupal-composer-project-templates)

For more information regarding docker4drupal stack please visit this [`link`](https://wodby.com/docs/1.0/stacks/drupal/local/)

### Run environment with Docker Compose

```
docker-compose up -d
```

Your drupal website should be up and running at http://drupal.docker.localhost:8000

You can see status of your containers and their logs via portainer: http://portainer.drupal.docker.localhost:8000

#### Drupal installation

Go through the steps of drupal installation by selecting the default options. The fourth step requires you to provide data to the database, provide the data according to the .env file.

#### Modules installation

The first necessary step to start working with the translation is to install the necessary modules: tmgmt and tmgmt_xtm.
Since tmgmt_xtm is dependent on tmgmt we first execute command:

```
# TMGMT module installation
docker-compose exec php composer require 'drupal/tmgmt:^1.15'
```

Login to drupal, go to Extend tab (/admin/modules), select and enable all items from Multilingual and Translation Management sections.

```
# TMGMT XTM module installation
docker-compose exec -w /var/www/html/web/modules/contrib php \
  git clone https://git.drupalcode.org/project/tmgmt_xtm.git
```

Go to Extend tab and enable newly added TMGMT XTM module.

#### Translation environment configuration

You can add new languages after navigating to configuration -> languages.

Configure new or edit already created translations provider in translation -> providers

```
# Default configuration:
# XTM API URL
# https://api-test.xtm-intl.com/project-manager-gui/services/v2/projectmanager/XTMWebService?wsdl
# XTM API Client name
# company_name
# XTM API User ID
# api_user_id
# XTM API Password
# api_password
# XTM project Customer ID
# api_customer_id
```

Select some sources to translate from the sources tab and add them to your cart or send them for translation.
