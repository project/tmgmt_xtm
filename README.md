# XTM Connect for Drupal

Drupal ver. 8.x-7.0.


Introduction
==============

XTM Connect for Drupal 8 is an auxiliary application for both XTM Cloud and XTM
Suite. It connects directly to your Drupal website, automating the process of
sending new or changed content to XTM and creating translation projects. When
the project workflow is complete; it returns translated content to the website.

- For a full description of the module, visit the project page:
  https://www.drupal.org/project/tmgmt_xtm
- To submit bug reports and feature suggestions, or to track changes: support@xtm.cloud


Requirements
==============

XTM Connect for Drupal 8 is installed as an additional module to Drupal 8.

It requires:

- Translation Management Tool ([TMGMT](https://www.drupal.org/project/tmgmt)) plugin
  to be installed and enabled on the Drupal website.
- PHP SOAP extension enabled in your server; For further information please go to SOAP
  setup documentation.
- XTM API account details. For more information, please contact XTM support at support@xtm.cloud.


Recommended modules
====================

- Entity API
- Views
- Chaos Tools (Base for Views)
- Views Bulk Operations
- Content Translation (for the content translation source)
- Locale
- Internationalization/i18n (for string translation)
- Entity translation (for the entity source)
- Rules (for node translation)


Installation
=============

Translation Management Tool and XTM Translator modules must be installed first
on the Drupal server, then in the Drupal interface.

Installation on server:

1. Download the latest version of Translation Management Tool module package for Drupal
   versions 8.x-x.x from https://www.drupal.org/project/tmgmt.
2. Extract the downloaded file in the modules folder of your Drupal installation.
3. Extract the XTM connector package received from XTM Sales in the modules folder of your
   Drupal installation.

Installation in Drupal:

1. Click Manage and go to the Extend tab.
2. Install the following modules:
   a. Content Entity Source
   b. Translation Management Core
   c. XTM Translator


Configuration
==============

Drupal 8 connector requires several configurations before content can be translated:

- Language configuration: Ensure languages are installed on your Drupal website.
To add languages, click _Languages_ on the Configuration Tab.
Translation is not possible when there are fewer than two languages installed on your website.
- Content language configuration: Content language settings depend on the
  Drupal languages available on your website.
Ensure the target languages are installed in your Drupal before enabling content language items.
- Provider configuration: a crucial step in the creation of connection between XTM and Drupal.
Providers are used to configure connections with XTM.
Multiple providers can be configured to offer a choice of different XTM set ups.
For detail/more information check our documentation.


Troubleshooting
================

- If you have purchased an XTM licence and it does not work, check if you have API enabled.
- If you cannot establish a connection with XTM, check if you have SOAP API enabled.
- If you cannot connect your Drupal instance to the sandbox server using Drupal connector,
  check if you have entered a correct XTM API URL in your configuration.
- If linguists do not receive e-mail notifications about Drupal-connected project commencements,
  check if the server address entered under the LSP->Connection tab is correct.
- If the language combination in the created project is different from the language in the template,
  check if language mapping in Drupal has been properly set
  (Administration -> Translation Management Translators -> Edit your translator -> Remote languages mapping)


FAQ
====

Q: Do I need to purchase an XTM licence to be able to use the Drupal plugin?
A: Yes.

Q: Do you have any demonstration videos about Drupal connection to XTM?
A: Yes. Please go to https://www.youtube.com/watch?v=bfrQoPEchW8&feature=youtu.be


Maintainers
=============
XTM International
