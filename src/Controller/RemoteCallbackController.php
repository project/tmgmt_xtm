<?php

namespace Drupal\tmgmt_xtm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt_xtm\Plugin\tmgmt\Translator\Connector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RemoteCallbackController.
 *
 * Handles remote callbacks for processing translation job IDs.
 *
 * @package Drupal\tmgmt_xtm\Controller
 */
class RemoteCallbackController extends ControllerBase {
  const TMGMT_JOB = 'tmgmt_job';
  const TMGMT_JOB_ITEM = 'tmgmt_job_item';
  const TMGMT_JOB_ID = 'xtmJobId';
  const XTM_PROJECT_ID = 'xtmProjectId';

  /**
   * Processes the callback request and queues the translation job data.
   *
   * This method is triggered by an external callback. It extracts the
   * translation job IDs and the XTM project ID from the request,
   * validates them, and then queues the job for further processing.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request containing the callback data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object indicating the outcome of the request processing.
   *   Returns a 400 response if the request is malformed.
   */
  public function callback(Request $request) {
    \Drupal::logger('xtm')->debug('Request received.');

    $tmgmtJobIDs = explode(',', htmlspecialchars_decode($_REQUEST[Connector::TMGMT_JOB_ID]));

    $data['xtmProjectId'] = (int) filter_var($_REQUEST[self::XTM_PROJECT_ID], FILTER_SANITIZE_NUMBER_INT);

    if (empty($data['xtmProjectId'])) {
      return new Response('Bad request.', 400);
    }

    $data['tmgmtJobIds'] = $tmgmtJobIDs;
    $data['xtmJobId'] = (int) filter_var($_REQUEST[self::TMGMT_JOB_ID], FILTER_SANITIZE_NUMBER_INT);

    $queue = \Drupal::queue('callback_job_queue');
    $queue->createQueue();
    $queue->createItem($data);

    return new Response();
  }

}
