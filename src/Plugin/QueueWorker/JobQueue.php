<?php

namespace Drupal\tmgmt_xtm\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\tmgmt_xtm\Controller\RemoteCallbackController;
use Drupal\tmgmt_xtm\Plugin\tmgmt\Translator\Connector;
use GuzzleHttp\Utils;

/**
 * Processes jobs from the callback queue.
 *
 * @QueueWorker(
 *   id = "callback_job_queue",
 *   title = @Translation("Callback Job queue"),
 *   cron = {"time" = 3600}
 * )
 */
class JobQueue extends QueueWorkerBase {

  /**
   * Processes a single item from the queue.
   *
   * @param array $data
   *   Data associated with the queue item, including job IDs
   *   and possibly an XTM job ID.
   */
  public function processItem($data) {
    foreach ($data['tmgmtJobIds'] as $jobId) {
      /** @var \Drupal\tmgmt\Entity\Job $job */
      $job = \Drupal::entityTypeManager()->getStorage(RemoteCallbackController::TMGMT_JOB)->load($jobId);

      $connector = new Connector();

      if (!$job) {
        try {
          /** @var \Drupal\tmgmt\Entity\JobItem $jobItem */
          $jobItem = \Drupal::entityTypeManager()->getStorage(RemoteCallbackController::TMGMT_JOB_ITEM)->load($jobId);
          $job = $jobItem->getJob();
          $reference = $connector->getReferenceFromRemoteMappings($jobItem);
        } catch (\RuntimeException $e) {
          \Drupal::logger('job_queue')
            ->error("TMGMT XTM Connector was unable to recognize job ID ". $jobId . ": " . Utils::jsonEncode($e->getMessage()));

          return;
        }
      } else {
        $reference = $job->getReference();
      }

      if ($this->isInvalidProjectInJob($data['xtmProjectId'], $reference)) {
        \Drupal::logger('job')->error('Invalid project in job ' . $jobId);
        return;
      }

      if (isset($data['xtmJobId'])) {
        $connector->setXtmJobId($data['xtmJobId']);
      }
      $job->isContinuous() ? $connector->retrieveContinuousTranslation($job) : $connector->retrieveTranslation($job);
    }
  }

  /**
   * Checks if the project referenced in the job is valid.
   *
   * @param $projectId
   *   The XTM project id from callback.
   * @param $reference
   *   The XTM project id stored as tmgmt job reference
   *
   * @return bool
   *   TRUE if the xtm project id in the job is invalid, FALSE otherwise.
   */
  private function isInvalidProjectInJob($projectId, $reference) {
    return $reference === NULL || (int) $reference !== (int) $projectId;
  }

}
