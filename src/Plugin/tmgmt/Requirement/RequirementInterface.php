<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Requirement;

/**
 * Defines an interface for checking specific requirements.
 *
 * This interface should be implemented by classes that need to verify
 * certain conditions or requirements, such as the availability of PHP
 * extensions during different phases of a process.
 */
interface RequirementInterface {

  /**
   * Checks if the requirement is met during the specified phase.
   *
   * @param string $phase
   *   The phase during which the check is performed, e.g., 'runtime'.
   *
   * @return mixed
   *   The return value depends on the implementing class. Typically, it could
   *   be an array of requirement details, a boolean indicating success, or
   *   another value based on the implementation.
   */
  public function check($phase);

}
