<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Requirement;

/**
 * Checks if the SOAP extension is enabled as a requirement.
 *
 * This class implements the RequirementInterface to verify whether the SOAP
 * extension is available in the current PHP environment during specific phases.
 */
class SoapRequirement implements RequirementInterface {

  /**
   * Checks if the SOAP extension is enabled during the specified phase.
   *
   * @param string $phase
   *   The phase during which the check is performed, e.g., 'runtime'.
   *
   * @return array
   *   An array of requirement details if SOAP is not enabled,
   *   otherwise an empty array.
   */
  public function check($phase) {
    if (($phase == 'runtime') && (!extension_loaded('soap'))) {
      $requirements['soap'] = [
        'title' => t('SOAP Disabled'),
        'description' => t('SOAP extension is disabled. Make sure the <a href=":url" target="_blank">PHP SOAP</a> package is installed.', [
          ':url' => 'https://www.php.net/manual/en/book.soap.php',
        ]),
        'severity' => REQUIREMENT_ERROR,
        'value' => t('Never run'),
      ];

      return $requirements;
    }

    return [];
  }

}
