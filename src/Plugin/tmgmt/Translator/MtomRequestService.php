<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Translator;

use Http\Message\MultipartStream\MultipartStreamBuilder;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Client\ClientInterface;

class MtomRequestService {
  protected $httpClient;
  protected $psr17Factory;
  protected $streamBuilder;

  const INTEGRATION_KEY = '163209fcd9394e34b625f371f66a0cb7';
  const XTM_API_URL = 'xtm_api_url';

  public function __construct(ClientInterface $httpClient, Psr17Factory $psr17Factory, MultipartStreamBuilder $multipartStreamBuilder) {
    $this->httpClient = $httpClient;
    $this->psr17Factory = $psr17Factory;
    $this->streamBuilder = $multipartStreamBuilder;
  }

  public function prepareTranslationFilesRequest($translator, $action, $input) {
    $loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/templates');
    $twig = new \Twig\Environment($loader);

    $projectRequest = $twig->render('translationFilesRequest.xml.twig', [
      'action' => $action,
      'translator' => $translator,
      'integration_key' => self::INTEGRATION_KEY,
      'input' => $input,
    ]);

    $this->streamBuilder->addResource('root', $projectRequest, [
      'headers' => [
        'Content-Type' => 'application/xop+xml; charset=UTF-8; type="text/xml"',
        'Content-Transfer-Encoding' => 'binary',
        'Content-ID' => '<root.message@cxf.apache.org>',
      ],
    ]);

    $multipartStream = $this->attachTranslationFiles($input);

    $request = $this->psr17Factory->createRequest('POST', $translator->getSetting(self::XTM_API_URL))
      ->withHeader('Content-Type', 'multipart/related; type="application/xop+xml"; boundary="' . $this->streamBuilder->getBoundary() . '"')
      ->withBody($this->psr17Factory->createStream($multipartStream));

    $this->streamBuilder->reset();

    return $request;
  }

  public function attachTranslationFiles($input) {
    foreach ($input['project']['translationFiles'] as $file) {
      $this->streamBuilder->addResource($file['fileName'], $file['fileMTOM'], [
        'headers' => [
          'Content-Type' => $file['contentType'],
          'Content-ID' => "<{$file['fileName']}>",
          'Content-Disposition' => sprintf('attachment; filename="%s"', $file['fileName'])
        ]
      ]);
    }

    return $this->streamBuilder->build();
  }

  public function sendTranslationFilesRequest($request) {
    return $this->httpClient->sendRequest($request);
  }
}
