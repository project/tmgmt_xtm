<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Translator;

use GuzzleHttp\Utils;

/**
 * Extend Helper class to provide multiple target project language in XTM.
 *
 * Class MultipleTargetLanguageHelper.
 *
 * @package Drupal\tmgmt_xtm\Plugin\tmgmt\Translator
 */
class MultipleTargetLanguageHelper extends Helper {

  /**
   * Creates a single XML file from the provided jobs.
   *
   * @param array $jobs
   *   An array of jobs to be included in the XML file.
   *
   * @return array
   *   An array containing the XML file data, including the file name,
   *   the XML content as a string, and external descriptors.
   */
  public function createSingleXMLFile($jobs) {
    [$keys, $job] = $this->createIDsForMultipleTranslation($jobs);
    $xml = new \SimpleXMLElement('<xtm-drupal-jobs></xtm-drupal-jobs>');
    $data = \Drupal::service('tmgmt.data')->filterTranslatable($this->getJobData($job));
    $data = $this->reorderItems($data);
    foreach ($data as $id => $text) {
      $str = $this->stripInvalidXml($text['#text']);
      $xml->addChild('xtm-drupal-job', htmlspecialchars($str))->addAttribute('id', $id);
    }
    $xml->addChild('xtm-drupal-multiple-job', '')->addAttribute('keys', Utils::jsonEncode($keys));

    return [
      [
        'fileName'            => $this->filterFileName($job->label(), $job->id()),
        'fileMTOM'            => $xml->asXML(),
        'externalDescriptors' => [],
      ],
    ];
  }

  /**
   * Generates unique IDs for multiple translations across different jobs.
   *
   * @param array $jobs
   *   An array of job entities from which to create IDs.
   *
   * @return array
   *   An array containing two elements:
   *   - An associative array where the keys are target language codes and the values are arrays of unique IDs.
   *   - The last processed job entity.
   */
  private function createIDsForMultipleTranslation($jobs) {
    $keys = [];
    $job = NULL;
    foreach ($jobs as $job) {
      /** @var \Drupal\tmgmt\Entity\Job $job */
      $dataP = \Drupal::service('tmgmt.data')->filterTranslatable($this->getJobData($job));
      $dataP = $this->reorderItems($dataP);
      foreach ($dataP as $idP => $textP) {
        $keys[$job->getTargetLangcode()][] = intval($idP);
      }
      $keys[$job->getTargetLangcode()] = array_unique($keys[$job->getTargetLangcode()]);
    }
    return [$keys, $job];
  }

  /**
   * Creates a multiple XML files from the provided jobs.
   *
   * @param array $jobs
   *   An array of jobs to be included in the XML files.
   *
   * @return array
   *   An array of XML files containing the XML file data, including the file name,
   *   the XML content as a string, and external descriptors.
   */
  public function createMultipleXMLFiles($jobs) {
    [$keys, $job] = $this->createIDsForMultipleTranslation($jobs);

    $files = [];
    $data = \Drupal::service('tmgmt.data')->filterTranslatable($this->getJobData($job));

    $data = $this->reorderItems($data);

    foreach ($data as $id => $text) {
      $xml = new \SimpleXMLElement('<xtm-drupal-jobs></xtm-drupal-jobs>');
      $xml->addChild('xtm-drupal-job', htmlspecialchars($text['#text']))->addAttribute('id', $id);
      $xml->addChild('xtm-drupal-multiple-job', '')->addAttribute('keys', Utils::jsonEncode($keys));

      $files[] = [
        'fileName'            => $this->filterFileName($job->label(), $id),
        'fileMTOM'            => $xml->asXML(),
        'externalDescriptors' => [],
      ];
    }

    return $files;
  }

}
