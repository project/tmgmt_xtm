<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Translator;

/**
 * Class GenericFilesProvider
 * @package Drupal\tmgmt_xtm\Plugin\tmgmt\Translator
 *
 * Provide any contained files from obtained XTM file
 */
class GenericFilesProvider
{
  /**
   * @var
   */
  private $xtmFile;

  /**
   * GenericFilesProvider constructor.
   * @param $xtmFile
   */
  public function __construct($xtmFile)
  {
    $this->xtmFile = $xtmFile;
  }

  /**
   * @return array
   * @throws \Exception
   */
  public function getFiles()
  {
    $this->unpack();
    $files = $this->loadFiles();
    $this->removeFolder();
    if (empty($files)) {
      throw new \Exception("Could not read file from Zip");
    }
    return $files;
  }

  /**
   *
   */
  private function unpack()
  {
    ZipHelper::unpack(FileHelper::getTemp($this->xtmFile), ZipHelper::get_unpack_folder($this->xtmFile));
  }

  /**
   * @return array|false
   * @throws \Exception
   */
  private function findPaths()
  {
    $globFilePaths = glob(ZipHelper::get_unpack_folder($this->xtmFile) . '/' . $this->xtmFile->targetLanguage . '/' . "*");
    if (empty($globFilePaths)) {
      $globFilePaths = glob(ZipHelper::get_unpack_folder($this->xtmFile) . '/' . "*");
    }
    if (empty($globFilePaths)) {
      throw new \Exception("Cannot resolve file path");
    }
    return $globFilePaths;
  }

  /**
   * @return array
   * @throws \Exception
   */
  private function loadFiles()
  {
    $xmls = [];
    foreach ($this->findPaths() as $globFilePath) {
      $content = file_get_contents($globFilePath);
      if (false === $content) {
        throw new \Exception("No DATA");
      }
      $xmls[pathinfo($globFilePath, PATHINFO_BASENAME)] = $content;
    }
    return $xmls;
  }

  /**
   *
   */
  private function removeFolder()
  {
    $filename = ZipHelper::get_unpack_folder($this->xtmFile);
    if (is_dir($filename)) {
      FileHelper::removeFolder($filename);
    }
  }
}
