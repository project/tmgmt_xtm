<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Translator;

/**
 * Class ZipHelper.
 *
 * Helper for extracting Zip files.
 *
 * @package Drupal\tmgmt_xtm\Plugin\tmgmt\Translator
 */
class ZipHelper {

  /**
   * Gets the path to the folder where the zip file will be unpacked.
   *
   * @param \stdClass $file
   *   The file object containing job descriptor information.
   *
   * @return string
   *   The path to the folder where the zip file will be unpacked.
   */
  public static function get_unpack_folder($file) {
    return \Drupal::service('file_system')->getTempDirectory()
      . '/' . $file->jobDescriptor->id;
  }

  /**
   * Unpacks a zip file to the specified target path.
   *
   * @param string $pathToZipFile
   *   The path to the zip file to be unpacked.
   * @param string $targetPath
   *   The path where the contents of the zip file will be extracted.
   */
  public static function unpack($pathToZipFile, $targetPath) {
    $zip = new \ZipArchive();
    $res = $zip->open($pathToZipFile);
    if ($res === TRUE) {
      $zip->extractTo($targetPath);
      $zip->close();
    }
  }

}
