<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Translator;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AbortItem.
 *
 * Provides functionality to handle the aborting of translation items.
 *
 * @package Drupal\tmgmt_xtm\Plugin\tmgmt\Translator
 */
class AbortItem {

  /**
   * Adds a form to handle aborting translation items.
   *
   * @param array $form
   *   The form array to be modified.
   */
  public function getForm(array &$form) {
    $form['fieldset'] = [
      '#type'   => 'container',
      '#prefix' => '<div id="templates-fieldset-wrapper">'
      . t('XTM Continuous projects will be moved to archived.'),
      '#suffix' => '</div>',
    ];
    $form['actions']['submit']['#submit'][] = 'tmgmt_xtm_form_tmgmt_job_item_abort_form_submit';
  }

  /**
   * Submits the form and updates the project activity if applicable.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state object containing the submitted form values.
   */
  public function submitForm(FormStateInterface $formState) {
    $formEntity = $formState->getFormObject();
    if (!$formEntity instanceof EntityFormInterface) {
      throw new \DomainException('The form entity must implement EntityFormInterface.');
    }
    /** @var \Drupal\tmgmt\Entity\JobItem $jobItem */
    $jobItem = $formEntity->getEntity();
    /** @var \Drupal\tmgmt\Entity\Translator $translator */
    $translator = $jobItem->getTranslator();
    $job = $jobItem->getJob();

    if ('xtm' === $translator->getPluginId() && $job->isContinuous()) {
      $connector = new Connector();
      $connector->updateProjectActivity($job);
    }
  }

}
