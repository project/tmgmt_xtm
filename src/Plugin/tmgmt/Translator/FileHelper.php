<?php

namespace Drupal\tmgmt_xtm\Plugin\tmgmt\Translator;

/**
 * Class FileHelper.
 *
 * Provides utility functions for file operations related to translation jobs.
 *
 * @package Drupal\tmgmt_xtm\Plugin\tmgmt\Translator
 */
class FileHelper {

  /**
   * Generates a temporary file path for a given translation file.
   *
   * This method constructs a temporary file path using the system's temporary
   * directory, appending the target language and file name.
   *
   * @param object $file
   *   The file object containing the target language and file name.
   *
   * @return string
   *   The full path to the temporary file location.
   */
  public static function getTemp($file) {
    return \Drupal::service('file_system')->getTempDirectory() .
      '/' . $file->targetLanguage . "_" . $file->fileName;
  }

  /**
   * Removes a directory and all its contents.
   *
   * This method recursively deletes all files and subdirectories within
   * the specified directory, then removes the directory itself.
   *
   * @param string $dir
   *   The path to the directory that needs to be removed.
   */
  public static function removeFolder($dir) {
    $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
    $files = new \RecursiveIteratorIterator($it, \RecursiveIteratorIterator::CHILD_FIRST);
    foreach ($files as $file) {
      if ($file->isDir()) {
        rmdir($file->getRealPath());
      }
      else {
        unlink($file->getRealPath());
      }
    }
    rmdir($dir);
  }

}
