<?php

namespace Drupal\tmgmt_xtm\Exception;

/**
 * Exception class for handling errors specific to the TMGMT XTM integration.
 *
 * This class extends the base Exception class and allows for custom exception
 * handling with dynamic message data insertion.
 */
class TMGMTXtmException extends \Exception {

  /**
   * Constructs a new TMGMTXtmException.
   *
   * @param string $message
   *   The exception message.
   * @param array $data
   *   Associative array of dynamic data that will be inserted into the message.
   * @param int $code
   *   The exception code (optional).
   * @param \Exception|null $previous
   *   The previous exception used for the exception chaining (optional).
   */
  public function __construct($message = "", array $data = [], $code = 0, \Exception $previous = NULL) {
    parent::__construct(strtr($message, $data), $code, $previous);
  }

}
